#include "desktop.h"
#include <QDesktopWidget>
#include <QIcon>
#include <QFileIconProvider>
#include <QFileInfo>
#include <QDir>
#include <QDebug>
#include <QMimeData>
#include <QPushButton>
#include "Button.h"
#include <QPainter>
#include <QThread>
#include "AddApp.h"
#include "xml.h"
#define DELAY 9
Desktop::Desktop(QWidget *parent)
	: QMainWindow(parent)
{
	
	ui.setupUi(this);
	this->setAcceptDrops(true);
	
	//获取屏幕的大小
	auto desktop = QApplication::desktop();
	m_desktop_w = desktop->width();
	m_desktop_h = desktop->height();

	//移动窗口到屏幕旁边
	this->move(QPoint(8- this->width(), m_desktop_h / 15));

	//设置窗口为顶级窗口   去标题栏
	this->setWindowFlags(Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
	this->setAttribute(Qt::WA_TranslucentBackground, true);
	//设置透明背景
	this->setWindowOpacity(1);

	//初始化按钮工厂
	ButtonFactory::Instance(ui.frame_button);
	auto factory = ButtonFactory::getInstance();
	factory->setLayoutType(ButtonFactory::medium);
	
	//保存窗口大小
	m_w = this->width();
	m_h = this->height();



	m_menu_flag = false;
	
	//设置右键菜单
	m_menu = new QMenu(this);
	m_add_app = new QAction(this);
	m_add_app->setText(QString::fromLocal8Bit("添加应用"));
	m_menu->addAction(m_add_app);
	connect(m_add_app, SIGNAL(triggered()), this, SLOT(addapp()));

	//加载窗口图片
	m_picture.load(":/re/Resources/bra.png");


	ui.frame->setMinimumHeight(this->height()-70);

	initButton();

	connect(ui.pushButton_close, SIGNAL(clicked()), this, SLOT(close()));
}

Desktop::~Desktop()
{

}

void Desktop::enterEvent(QEvent * event)
{
	switch (m_flag_hide)
	{
		case 1:
			this->move(pos().x(), -2);
			break;
		case 2:
			this->move(-2, pos().y());
			break;
		case 3:
			this->move(m_desktop_w - this->width() + 2, pos().y());
			break;
		default:
			break;
	}
}

void Desktop::leaveEvent(QEvent * event)
{
	//QThread::msleep(500);

	QPoint pos = this->pos();
	if (pos.x()<2)
	{
		//靠左边
		if (pos.y()>5)
		{
			double f = 0.0;
			int t =pos.x()-(-this->width()+8);
			int t_x = pos.x();
			while (1)
			{
				qDebug() << t_x -t*(f / 50) << endl;
				this->move(t_x-t*(f/50), pos.y());
				f++;
				QThread::msleep(DELAY);
				if (f>50)
				{
					break;
				}
			}
			m_flag_hide = 2;
		}
		//靠上边
		else
		{
			m_flag_hide = 1;
			double f = 0.0;
			int t = (-(this->height() - 4)) - pos.y();
			int pos_y = pos.y();
			while (1)
			{
				this->move(pos.x(), pos_y + t*(f / 50));
				f++;
				QThread::msleep(DELAY);
				if (f >50)
				{
					break;
				}
			}
			
		}
	}
	else if (pos.x() > m_desktop_w - this->width() - 2)
	{
		//靠右边
		if (pos.y() > 5)
		{
			m_flag_hide = 3;
			double f = 0.0;
			int t = m_desktop_w-5-pos.x();
			int pos_x = pos.x();
			while (1)
			{
				this->move(pos_x+t*(f/50), pos.y());
				f++;
				QThread::msleep(DELAY);
				if (f > 50)
				{
					break;
				}
			}
		}
		//靠上边
		else
		{
			m_flag_hide = 1;
			double f = 0.0;
			int t = (-(this->height() - 4)) - pos.y();
			int pos_y = pos.y();
			while (1)
			{
				this->move(pos.x(), pos_y + t*(f / 50));
				f++;
				QThread::msleep(DELAY);
				if (f>50)
				{
					break;
				}
			}
		}
	}
	//上边
	else if (pos.y() < 2)
	{
		m_flag_hide = 1;
		double f = 0.0;
		int t =(-(this->height() - 4))-pos.y();
		int pos_y = pos.y();
		while (1)
		{
			this->move(pos.x(),pos_y +t*(f / 50));
			f++;
			QThread::msleep(DELAY);
			if (f>50)
			{
				break;
			}
		}
	}
	//不移动
	else
	{
		m_flag_hide = 0;
	}
}

void Desktop::dragEnterEvent(QDragEnterEvent * event)
{
	auto t = event->mimeData();
	auto urls=t->urls();
	if (t->hasUrls())
	{
		auto factory = ButtonFactory::getInstance();
		auto str = urls[0].path();
		qDebug() << str << endl;
		auto name = str.section("/", -1).remove(QRegExp(".exe|.lnk"));
		auto path = str.mid(1, str.length());
		QFileInfo info(path);
		if (info.isSymLink())
		{
			path = info.symLinkTarget();
		}
		AppPathData d;
		d.app_ioc_path = path;
		d.app_name = name;
		d.app_path = path;

		unsigned int id= GetTickCount();
		Xml::getInstance()->addAppData(id,d);

		//需要  C:/XXX/XX.X      
		auto b = factory->creatButton(id,path,path,name);
		connect(b, SIGNAL(reload()), this, SLOT(reLoadButton()));
		b->show();
		event->acceptProposedAction();
	}
	
}

void Desktop::contextMenuEvent(QContextMenuEvent *event)
{
	if (event->reason()==QContextMenuEvent::Mouse)
	{
		m_menu->exec(event->globalPos());
	}
}

void Desktop::mouseMoveEvent(QMouseEvent * event)
{
	if (event->buttons() & Qt::LeftButton)   //注意buttons（）    
	{
		if (m_dragging)
		{
			QPoint data = event->globalPos() - m_startPos;
			qDebug() << data;
			move(m_framePos + data);

		}
	}
	QWidget::mouseMoveEvent(event);
}

void Desktop::mousePressEvent(QMouseEvent * event)
{
	if (event->button() == Qt::LeftButton)
	{
		m_dragging = true;
		m_startPos = event->globalPos();
		m_framePos = frameGeometry().topLeft();

	}
	QWidget::mousePressEvent(event);
}

void Desktop::mouseReleaseEvent(QMouseEvent * event)
{
	if (event->button() == Qt::LeftButton)
	{
		m_dragging = false;
	}
	
	
	QWidget::mouseReleaseEvent(event);
}

void Desktop::paintEvent(QPaintEvent*event)
{
	QPainter painter(this);
	QRect frameRect = rect();
	//m_picture = m_picture.scaled(QSize(this->width(), this->height()+5), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
	painter.drawPixmap(0, 0, this->width(), this->height()+5, m_picture);

}

void Desktop::initButton()
{

	auto f=ButtonFactory::getInstance();
	m_appDataMap.clear();
	if (Xml::getInstance()->getAppData(m_appDataMap)==0)
	{
		for (auto i : m_appDataMap)
		{
			auto b=f->creatButton(i.first,i.second.app_path,i.second.app_ioc_path,i.second.app_name);
			b->show();
			connect(b, SIGNAL(reload()), this, SLOT(reLoadButton()));
		}
	}
}

void Desktop::menu_open_close()
{
}

void Desktop::addapp()
{
	AddApp dialog(this);
	connect(&dialog, SIGNAL(sendData(AppPathData)), this, SLOT(addappdata(AppPathData)));
	dialog.exec();
	
}

void Desktop::addappdata(AppPathData data)
{
	m_appDataMap.insert(std::map<unsigned int, AppPathData>::value_type(GetTickCount(), data));
	auto factory = ButtonFactory::getInstance();
	//需要  C:/XXX/XX.X   
	unsigned int id = GetTickCount();
	auto b = factory->creatButton(id,data.app_path,data.app_ioc_path,data.app_name);
	connect(b, SIGNAL(reload()), this, SLOT(reLoadButton()));
	Xml::getInstance()->addAppData(id, data);
	b->show();
}

void Desktop::reLoadButton()
{
	ButtonFactory::getInstance()->clear();
	ui.frame_button->update();
	ui.frame_button->show();
	this->initButton();
}
