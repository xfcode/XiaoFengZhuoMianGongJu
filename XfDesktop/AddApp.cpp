#include "AddApp.h"
#include <QFileDialog>
#include <QDebug>
AddApp::AddApp(QWidget *parent)
: QDialog(parent), data({"","",""})
{
	ui.setupUi(this);
	m_pag_main_curr = 0;
	ui.stackedWidget_main->setCurrentIndex(m_pag_main_curr);
	ui.stackedWidget_button->setCurrentIndex(0);

	connect(ui.next_button,SIGNAL(clicked()),this,SLOT(nextButton()));
	connect(ui.last_button, SIGNAL(clicked()), this, SLOT(lastButton()));
	connect(ui.finsh_button, SIGNAL(clicked()), this, SLOT(finshButton()));
	connect(ui.path_app_button, SIGNAL(clicked()), this, SLOT(appPathButton()));
	connect(ui.pushButton_app_ioc, SIGNAL(clicked()), this, SLOT(appIocButton()));
}

AddApp::~AddApp()
{

}

void AddApp::nextButton()
{
	int tmp = m_pag_main_curr+1;
	if (tmp > 2)
	{
		return;
	}
	ui.stackedWidget_main->setCurrentIndex(++m_pag_main_curr);
	if (m_pag_main_curr==1)
	{
		data.app_name = data.app_path.section("/", -1);
		ui.lineEdit_app_name->setText(data.app_name);
	}
	if (m_pag_main_curr==2)
	{
		ui.stackedWidget_button->setCurrentIndex(1);
	}
	if (m_pag_main_curr==3)
	{
// 		if (!data.app_ioc_path.isEmpty())
// 		{
// 			QString tmp = data.app_ioc_path;
// 			data.app_ioc_path = QCoreApplication::applicationDirPath() + "/icon/" +
// 				data.app_ioc_path.section("/", -1);
// 			qDebug() << data.app_ioc_path << endl;
// 			QFile::copy(tmp,data.app_ioc_path );
// 		}
// 		ui.stackedWidget_button->setCurrentIndex(1);
	}
}

void AddApp::lastButton()
{
	int tmp = m_pag_main_curr-1;
	if (tmp<0)
	{
		return;
	}
	ui.stackedWidget_main->setCurrentIndex(--m_pag_main_curr);
}

void AddApp::finshButton()
{
	data.app_name = ui.lineEdit_app_name->text();
	data.app_path = ui.lineEdit_path_app->text();
	data.app_ioc_path = data.app_path;
	//data.app_ioc_path = ui.lineEdit_app_ioc->text();
	emit sendData(data);
	this->accept();
}

void AddApp::appPathButton()
{
	QFileDialog *fileDialog = new QFileDialog(this);
	fileDialog->setWindowTitle(QString::fromLocal8Bit("选择应用"));
	fileDialog->setDirectory(".");
	fileDialog->setFileMode(QFileDialog::ExistingFiles);
	fileDialog->setViewMode(QFileDialog::Detail);
	
	if (fileDialog->exec())
	{
		data.app_path = fileDialog->directory().filePath(fileDialog->selectedFiles()[0]);
		qDebug() << data.app_path << endl;
	}
	ui.lineEdit_path_app->setText(data.app_path);
}

void AddApp::appIocButton()
{
// 	QFileDialog *fileDialog = new QFileDialog(this);
// 	fileDialog->setWindowTitle(QString::fromLocal8Bit("选择图标资源"));
// 	fileDialog->setDirectory(".");
// 	fileDialog->setNameFilter(QString::fromLocal8Bit("Images(*.png *.jpg *.jpeg *.bmp)"));
// 	fileDialog->setFileMode(QFileDialog::ExistingFiles);
// 	fileDialog->setViewMode(QFileDialog::Detail);
// 
// 	if (fileDialog->exec())
// 	{
// 		data.app_ioc_path = fileDialog->directory().filePath(fileDialog->selectedFiles()[0]);
// 		qDebug() << data.app_ioc_path << endl;
// 	}
// 	ui.lineEdit_app_ioc->setText(data.app_ioc_path);
}
