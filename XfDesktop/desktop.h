#ifndef DESKTOP_H
#define DESKTOP_H

#include <QtWidgets/QMainWindow>
#include "ui_desktop.h"
#include <QMenu>
#include "type.h"
#include <map>
class Desktop : public QMainWindow
{
	Q_OBJECT

public:
	Desktop(QWidget *parent = 0);
	~Desktop();

private:
	Ui::DesktopClass ui;

	virtual void	enterEvent(QEvent * event);
	virtual void	leaveEvent(QEvent * event);
	virtual void	dragEnterEvent(QDragEnterEvent * event);
	virtual void	contextMenuEvent(QContextMenuEvent *event);
	virtual void	mouseMoveEvent(QMouseEvent * event);
	virtual void	mousePressEvent(QMouseEvent * event);
	virtual void	mouseReleaseEvent(QMouseEvent * event);
	virtual void	paintEvent(QPaintEvent*event);
private:
	int m_desktop_w;
	int m_desktop_h;
	int m_w;   //程序窗口的大小  在不计算菜单窗口的情况下
	int m_h;
	QPixmap m_picture; 
	/******窗口拖动******/
	bool m_dragging; //是否移动 
	QPoint m_startPos;  //拖动的开始坐标 
	QPoint m_framePos;   //窗体的原始坐标 

	bool m_menu_flag;   //菜单是否打开

	int m_flag_hide;  //隐藏的标志  1 上  2 左 3 右
	QMenu *  m_menu;
	QAction * m_add_app;
	std::map<unsigned int, AppPathData> m_appDataMap;
private:
	void initButton();
private slots:
void menu_open_close();
void addapp();
void addappdata(AppPathData data);
void reLoadButton();
};

#endif // DESKTOP_H
