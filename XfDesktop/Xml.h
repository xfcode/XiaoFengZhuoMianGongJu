#pragma once
#include "tinyxml/AfTinyXml.h"
#include "tinyxml/tinyxml.h"
#include "type.h"
#include <map>
#include <string>

const std::string APPDADAFILE = "appData.xml";
const std::string APPCONFIGFILE = "config.xml";

class Xml
{
public:
	Xml();
	~Xml();

	static Xml* getInstance();

	//添加app信息
	int addAppData(unsigned int id,AppPathData data);
	//删除app的信息
	int removeAppData(unsigned int id);
	//获取app信息
	//@dataMap    传入需要获取数据的引用
	// return  函数执行情况   -1 出错  0 正常
	int getAppData(std::map<unsigned int ,AppPathData>& dataMap);
	
private:
	static Xml* g_xml;
};

