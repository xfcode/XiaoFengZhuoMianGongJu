/********************************************************************************
** Form generated from reading UI file 'desktop.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DESKTOP_H
#define UI_DESKTOP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DesktopClass
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QFrame *frame;
    QVBoxLayout *verticalLayout_5;
    QFrame *frame_main;
    QVBoxLayout *verticalLayout_2;
    QFrame *frame_button;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_3;
    QFrame *line;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *verticalSpacer_5;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_5;
    QFrame *frame_2;
    QHBoxLayout *horizontalLayout_3;
    QLineEdit *lineEdit;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer_6;
    QSpacerItem *verticalSpacer_4;
    QLabel *label;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_2;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *pushButton_close;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer_3;

    void setupUi(QMainWindow *DesktopClass)
    {
        if (DesktopClass->objectName().isEmpty())
            DesktopClass->setObjectName(QStringLiteral("DesktopClass"));
        DesktopClass->resize(257, 584);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(DesktopClass->sizePolicy().hasHeightForWidth());
        DesktopClass->setSizePolicy(sizePolicy);
        QIcon icon;
        icon.addFile(QStringLiteral(":/re/Resources/app.png"), QSize(), QIcon::Normal, QIcon::Off);
        DesktopClass->setWindowIcon(icon);
        DesktopClass->setStyleSheet(QStringLiteral(""));
        centralWidget = new QWidget(DesktopClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        sizePolicy.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy);
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(10, 11, 10, 5);
        frame = new QFrame(centralWidget);
        frame->setObjectName(QStringLiteral("frame"));
        sizePolicy.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy);
        frame->setStyleSheet(QLatin1String("QFrame#frame{border: 1px solid;\n"
"border-radius:2px;\n"
"border-color: rgb(76, 130, 255);}"));
        frame->setFrameShape(QFrame::Box);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout_5 = new QVBoxLayout(frame);
        verticalLayout_5->setSpacing(0);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        frame_main = new QFrame(frame);
        frame_main->setObjectName(QStringLiteral("frame_main"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame_main->sizePolicy().hasHeightForWidth());
        frame_main->setSizePolicy(sizePolicy1);
        frame_main->setMinimumSize(QSize(235, 566));
        frame_main->setMaximumSize(QSize(16777215, 565));
        frame_main->setFrameShape(QFrame::StyledPanel);
        frame_main->setFrameShadow(QFrame::Raised);
        verticalLayout_2 = new QVBoxLayout(frame_main);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        frame_button = new QFrame(frame_main);
        frame_button->setObjectName(QStringLiteral("frame_button"));
        frame_button->setMinimumSize(QSize(0, 440));
        frame_button->setFrameShape(QFrame::StyledPanel);
        frame_button->setFrameShadow(QFrame::Raised);

        verticalLayout_2->addWidget(frame_button);

        verticalSpacer = new QSpacerItem(20, 377, QSizePolicy::Minimum, QSizePolicy::Preferred);

        verticalLayout_2->addItem(verticalSpacer);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        line = new QFrame(frame_main);
        line->setObjectName(QStringLiteral("line"));
        line->setStyleSheet(QLatin1String("border-top: 1px solid;\n"
"border-top-color: rgb(74, 128, 255);\n"
""));
        line->setFrameShadow(QFrame::Sunken);
        line->setFrameShape(QFrame::HLine);

        horizontalLayout_2->addWidget(line);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);


        verticalLayout_2->addLayout(horizontalLayout_2);

        verticalSpacer_5 = new QSpacerItem(20, 10, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_2->addItem(verticalSpacer_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalSpacer_5 = new QSpacerItem(20, 10, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_5);

        frame_2 = new QFrame(frame_main);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setMinimumSize(QSize(200, 0));
        frame_2->setStyleSheet(QLatin1String("QFrame#frame_2{border: 1px solid;\n"
"border-radius:2px;\n"
"border-color: rgb(76, 130, 255);}"));
        frame_2->setFrameShape(QFrame::Box);
        frame_2->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_2);
        horizontalLayout_3->setSpacing(0);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        lineEdit = new QLineEdit(frame_2);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setMinimumSize(QSize(0, 25));
        lineEdit->setStyleSheet(QLatin1String("border-width:0;border-style:outset;\n"
"background-color: rgba(255, 255, 255, 125);\n"
"color: rgb(0, 0, 0);\n"
""));
        lineEdit->setEchoMode(QLineEdit::Normal);

        horizontalLayout_3->addWidget(lineEdit);

        pushButton = new QPushButton(frame_2);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setMinimumSize(QSize(35, 25));
        pushButton->setCursor(QCursor(Qt::PointingHandCursor));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/re/Resources/select.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton->setIcon(icon1);
        pushButton->setFlat(true);

        horizontalLayout_3->addWidget(pushButton);


        horizontalLayout_6->addWidget(frame_2);

        horizontalSpacer_6 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_6);


        verticalLayout_2->addLayout(horizontalLayout_6);

        verticalSpacer_4 = new QSpacerItem(20, 10, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_2->addItem(verticalSpacer_4);

        label = new QLabel(frame_main);
        label->setObjectName(QStringLiteral("label"));
        label->setStyleSheet(QString::fromUtf8("color: rgb(66, 66, 66);\n"
"background-color: qconicalgradient(cx:0.983, cy:0.0284091, angle:0, stop:0.4375 rgba(0, 127, 157, 247), stop:0.886364 rgba(255, 255, 255, 255));\n"
"font: 11pt \"\345\215\216\346\226\207\346\226\260\351\255\217\";\n"
"border:2xp solid;\n"
"border-color: rgb(83, 149, 255);"));
        label->setFrameShape(QFrame::Box);
        label->setFrameShadow(QFrame::Plain);
        label->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label);

        verticalSpacer_2 = new QSpacerItem(20, 7, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_2->addItem(verticalSpacer_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(13, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_2 = new QPushButton(frame_main);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setCursor(QCursor(Qt::PointingHandCursor));
        pushButton_2->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"    border: 1px solid rgb(85, 170, 255);\n"
"    border-radius:2px;\n"
"    min-width: 75px;\n"
"	min-height: 23px;\n"
"	color: rgb(18, 150, 219);\n"
"	font: 75 12pt \"\345\215\216\346\226\207\346\226\260\351\255\217\";\n"
"}\n"
":hover{\n"
"	border: 1px solid rgb(7, 146, 184);\n"
"    border-radius:2px;\n"
"    min-width: 75px;\n"
"	min-height: 23px;\n"
"}\n"
":pressed{\n"
"	color: #e6e6e6;\n"
"}\n"
""));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/re/Resources/\350\256\276\347\275\256.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon2.addFile(QString::fromUtf8(":/re/Resources/\350\256\276\347\275\256.png"), QSize(), QIcon::Selected, QIcon::On);
        pushButton_2->setIcon(icon2);

        horizontalLayout->addWidget(pushButton_2);

        horizontalSpacer_7 = new QSpacerItem(18, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_7);

        pushButton_close = new QPushButton(frame_main);
        pushButton_close->setObjectName(QStringLiteral("pushButton_close"));
        pushButton_close->setCursor(QCursor(Qt::PointingHandCursor));
        pushButton_close->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"    border: 1px solid rgb(85, 170, 255);\n"
"    border-radius:2px;\n"
"    min-width: 75px;\n"
"	min-height: 23px;\n"
"	color: rgb(18, 150, 219);\n"
"	font: 75 12pt \"\345\215\216\346\226\207\346\226\260\351\255\217\";\n"
"}\n"
":hover{\n"
"	border: 1px solid rgb(7, 146, 184);\n"
"    border-radius:2px;\n"
"    min-width: 75px;\n"
"	min-height: 23px;\n"
"}\n"
":pressed{\n"
"	color: #e6e6e6;\n"
"}\n"
""));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/re/Resources/\351\200\200\345\207\272.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_close->setIcon(icon3);

        horizontalLayout->addWidget(pushButton_close);

        horizontalSpacer_2 = new QSpacerItem(17, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout_2->addLayout(horizontalLayout);

        verticalSpacer_3 = new QSpacerItem(20, 7, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_2->addItem(verticalSpacer_3);


        verticalLayout_5->addWidget(frame_main);


        verticalLayout->addWidget(frame);

        DesktopClass->setCentralWidget(centralWidget);

        retranslateUi(DesktopClass);

        QMetaObject::connectSlotsByName(DesktopClass);
    } // setupUi

    void retranslateUi(QMainWindow *DesktopClass)
    {
        DesktopClass->setWindowTitle(QApplication::translate("DesktopClass", "Desktop", 0));
        pushButton->setText(QString());
        label->setText(QApplication::translate("DesktopClass", "\345\260\217\351\243\216\346\241\214\351\235\242\345\267\245\345\205\267", 0));
        pushButton_2->setText(QApplication::translate("DesktopClass", "\350\256\276\347\275\256", 0));
        pushButton_close->setText(QApplication::translate("DesktopClass", "\351\200\200\345\207\272", 0));
    } // retranslateUi

};

namespace Ui {
    class DesktopClass: public Ui_DesktopClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DESKTOP_H
