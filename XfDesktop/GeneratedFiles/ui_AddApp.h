/********************************************************************************
** Form generated from reading UI file 'AddApp.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDAPP_H
#define UI_ADDAPP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AddApp
{
public:
    QVBoxLayout *verticalLayout;
    QStackedWidget *stackedWidget_main;
    QWidget *page;
    QVBoxLayout *verticalLayout_2;
    QSpacerItem *verticalSpacer_3;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_8;
    QLabel *label;
    QSpacerItem *horizontalSpacer_9;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_6;
    QLineEdit *lineEdit_path_app;
    QPushButton *path_app_button;
    QSpacerItem *horizontalSpacer_7;
    QSpacerItem *verticalSpacer_2;
    QWidget *page_2;
    QVBoxLayout *verticalLayout_3;
    QSpacerItem *verticalSpacer_4;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_10;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_11;
    QSpacerItem *verticalSpacer_5;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_12;
    QLineEdit *lineEdit_app_name;
    QSpacerItem *horizontalSpacer_13;
    QSpacerItem *verticalSpacer_6;
    QWidget *page_6;
    QWidget *page_5;
    QVBoxLayout *verticalLayout_4;
    QSpacerItem *verticalSpacer_7;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_14;
    QLabel *label_3;
    QSpacerItem *horizontalSpacer_15;
    QSpacerItem *verticalSpacer_11;
    QSpacerItem *verticalSpacer_8;
    QHBoxLayout *horizontalLayout_8;
    QSpacerItem *horizontalSpacer_17;
    QLineEdit *lineEdit_app_ioc;
    QPushButton *pushButton_app_ioc;
    QSpacerItem *horizontalSpacer_16;
    QSpacerItem *verticalSpacer_10;
    QHBoxLayout *horizontalLayout_9;
    QSpacerItem *horizontalSpacer_19;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer_18;
    QSpacerItem *verticalSpacer_9;
    QFrame *line;
    QStackedWidget *stackedWidget_button;
    QWidget *page_3;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *last_button;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *next_button;
    QSpacerItem *horizontalSpacer_3;
    QWidget *page_4;
    QWidget *layoutWidget1;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *finsh_button;
    QSpacerItem *horizontalSpacer_5;

    void setupUi(QDialog *AddApp)
    {
        if (AddApp->objectName().isEmpty())
            AddApp->setObjectName(QStringLiteral("AddApp"));
        AddApp->resize(330, 230);
        verticalLayout = new QVBoxLayout(AddApp);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        stackedWidget_main = new QStackedWidget(AddApp);
        stackedWidget_main->setObjectName(QStringLiteral("stackedWidget_main"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(stackedWidget_main->sizePolicy().hasHeightForWidth());
        stackedWidget_main->setSizePolicy(sizePolicy);
        page = new QWidget();
        page->setObjectName(QStringLiteral("page"));
        verticalLayout_2 = new QVBoxLayout(page);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalSpacer_3 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_2->addItem(verticalSpacer_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_8);

        label = new QLabel(page);
        label->setObjectName(QStringLiteral("label"));
        label->setStyleSheet(QStringLiteral("font: 11pt \"Agency FB\";"));

        horizontalLayout_4->addWidget(label);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_9);


        verticalLayout_2->addLayout(horizontalLayout_4);

        verticalSpacer = new QSpacerItem(20, 21, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);

        lineEdit_path_app = new QLineEdit(page);
        lineEdit_path_app->setObjectName(QStringLiteral("lineEdit_path_app"));
        lineEdit_path_app->setMinimumSize(QSize(0, 30));

        horizontalLayout_3->addWidget(lineEdit_path_app);

        path_app_button = new QPushButton(page);
        path_app_button->setObjectName(QStringLiteral("path_app_button"));
        path_app_button->setMinimumSize(QSize(0, 30));

        horizontalLayout_3->addWidget(path_app_button);

        horizontalSpacer_7 = new QSpacerItem(28, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_7);


        verticalLayout_2->addLayout(horizontalLayout_3);

        verticalSpacer_2 = new QSpacerItem(20, 21, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);

        stackedWidget_main->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QStringLiteral("page_2"));
        verticalLayout_3 = new QVBoxLayout(page_2);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalSpacer_4 = new QSpacerItem(20, 21, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_3->addItem(verticalSpacer_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_10);

        label_2 = new QLabel(page_2);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setStyleSheet(QStringLiteral("font: 11pt \"Agency FB\";"));

        horizontalLayout_5->addWidget(label_2);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_11);


        verticalLayout_3->addLayout(horizontalLayout_5);

        verticalSpacer_5 = new QSpacerItem(20, 21, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_12);

        lineEdit_app_name = new QLineEdit(page_2);
        lineEdit_app_name->setObjectName(QStringLiteral("lineEdit_app_name"));
        lineEdit_app_name->setMinimumSize(QSize(200, 30));

        horizontalLayout_6->addWidget(lineEdit_app_name);

        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_13);


        verticalLayout_3->addLayout(horizontalLayout_6);

        verticalSpacer_6 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_6);

        stackedWidget_main->addWidget(page_2);
        page_6 = new QWidget();
        page_6->setObjectName(QStringLiteral("page_6"));
        stackedWidget_main->addWidget(page_6);
        page_5 = new QWidget();
        page_5->setObjectName(QStringLiteral("page_5"));
        verticalLayout_4 = new QVBoxLayout(page_5);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalSpacer_7 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_4->addItem(verticalSpacer_7);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalSpacer_14 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_14);

        label_3 = new QLabel(page_5);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setStyleSheet(QStringLiteral("font: 11pt \"Agency FB\";"));

        horizontalLayout_7->addWidget(label_3);

        horizontalSpacer_15 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_15);


        verticalLayout_4->addLayout(horizontalLayout_7);

        verticalSpacer_11 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_11);

        verticalSpacer_8 = new QSpacerItem(20, 5, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_8);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        horizontalSpacer_17 = new QSpacerItem(13, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_17);

        lineEdit_app_ioc = new QLineEdit(page_5);
        lineEdit_app_ioc->setObjectName(QStringLiteral("lineEdit_app_ioc"));
        lineEdit_app_ioc->setMinimumSize(QSize(0, 30));

        horizontalLayout_8->addWidget(lineEdit_app_ioc);

        pushButton_app_ioc = new QPushButton(page_5);
        pushButton_app_ioc->setObjectName(QStringLiteral("pushButton_app_ioc"));
        pushButton_app_ioc->setMinimumSize(QSize(0, 30));

        horizontalLayout_8->addWidget(pushButton_app_ioc);

        horizontalSpacer_16 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_16);


        verticalLayout_4->addLayout(horizontalLayout_8);

        verticalSpacer_10 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_10);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        horizontalSpacer_19 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_19);

        label_4 = new QLabel(page_5);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_9->addWidget(label_4);

        horizontalSpacer_18 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_18);


        verticalLayout_4->addLayout(horizontalLayout_9);

        verticalSpacer_9 = new QSpacerItem(20, 9, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_9);

        stackedWidget_main->addWidget(page_5);

        verticalLayout->addWidget(stackedWidget_main);

        line = new QFrame(AddApp);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        stackedWidget_button = new QStackedWidget(AddApp);
        stackedWidget_button->setObjectName(QStringLiteral("stackedWidget_button"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(stackedWidget_button->sizePolicy().hasHeightForWidth());
        stackedWidget_button->setSizePolicy(sizePolicy1);
        stackedWidget_button->setMinimumSize(QSize(0, 30));
        stackedWidget_button->setMaximumSize(QSize(16777215, 30));
        page_3 = new QWidget();
        page_3->setObjectName(QStringLiteral("page_3"));
        layoutWidget = new QWidget(page_3);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(0, 0, 312, 25));
        horizontalLayout = new QHBoxLayout(layoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        last_button = new QPushButton(layoutWidget);
        last_button->setObjectName(QStringLiteral("last_button"));

        horizontalLayout->addWidget(last_button);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        next_button = new QPushButton(layoutWidget);
        next_button->setObjectName(QStringLiteral("next_button"));

        horizontalLayout->addWidget(next_button);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        stackedWidget_button->addWidget(page_3);
        page_4 = new QWidget();
        page_4->setObjectName(QStringLiteral("page_4"));
        layoutWidget1 = new QWidget(page_4);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(10, 0, 281, 31));
        horizontalLayout_2 = new QHBoxLayout(layoutWidget1);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);

        finsh_button = new QPushButton(layoutWidget1);
        finsh_button->setObjectName(QStringLiteral("finsh_button"));

        horizontalLayout_2->addWidget(finsh_button);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_5);

        stackedWidget_button->addWidget(page_4);

        verticalLayout->addWidget(stackedWidget_button);


        retranslateUi(AddApp);

        stackedWidget_main->setCurrentIndex(3);
        stackedWidget_button->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(AddApp);
    } // setupUi

    void retranslateUi(QDialog *AddApp)
    {
        AddApp->setWindowTitle(QApplication::translate("AddApp", "AddApp", 0));
        label->setText(QApplication::translate("AddApp", "\347\254\254\344\270\200\346\255\245\357\274\232\351\200\211\346\213\251\346\226\207\344\273\266\350\267\257\345\276\204", 0));
        path_app_button->setText(QApplication::translate("AddApp", "\351\200\211\346\213\251\346\226\207\344\273\266", 0));
        label_2->setText(QApplication::translate("AddApp", "\347\254\254\344\272\214\346\255\245\357\274\232\350\276\223\345\205\245\345\272\224\347\224\250\345\220\215\347\247\260", 0));
        label_3->setText(QApplication::translate("AddApp", "\347\254\254\344\270\211\346\255\245\357\274\232\351\200\211\346\213\251\345\233\276\346\240\207", 0));
        pushButton_app_ioc->setText(QApplication::translate("AddApp", "\351\200\211\346\213\251\346\226\207\344\273\266", 0));
        label_4->setText(QApplication::translate("AddApp", "\346\263\250\357\274\232\346\226\207\344\273\266\345\260\272\345\257\270\346\216\250\350\215\220\344\270\272 60*60", 0));
        last_button->setText(QApplication::translate("AddApp", "\350\277\224\345\233\236", 0));
        next_button->setText(QApplication::translate("AddApp", "\344\270\213\344\270\200\346\255\245", 0));
        finsh_button->setText(QApplication::translate("AddApp", "\345\256\214\346\210\220", 0));
    } // retranslateUi

};

namespace Ui {
    class AddApp: public Ui_AddApp {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDAPP_H
