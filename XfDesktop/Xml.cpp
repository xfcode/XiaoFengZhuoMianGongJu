#include "Xml.h"


Xml::Xml()
{
}


Xml::~Xml()
{
}

Xml* Xml::getInstance()
{
	if (g_xml==nullptr)
	{
		g_xml = new Xml();
	}
	return g_xml;
}

int Xml::addAppData(unsigned int id, AppPathData data)
{
	TiXmlDocument xml_doc;
	if (!xml_doc.LoadFile(APPDADAFILE))
	{
		return -1;
	}
	TiXmlElement*xml_root = xml_doc.RootElement();
	TiXmlElement newNode("infor");
	TiXmlElement name ("name");
	newNode.InsertEndChild(name)->InsertEndChild(TiXmlText(data.app_name.toStdString()));
	TiXmlElement path("path");
	newNode.InsertEndChild(path)->InsertEndChild(TiXmlText(data.app_path.toStdString()));
	TiXmlElement icon("icon");
	newNode.InsertEndChild(icon)->InsertEndChild(TiXmlText(data.app_ioc_path.toStdString()));
	TiXmlElement ids("id");
	char buf[128]{0};
	sprintf_s(buf, 128, "%u", id);
	newNode.InsertEndChild(ids)->InsertEndChild(TiXmlText(buf));
	xml_root->InsertEndChild(newNode);
	xml_doc.SaveFile();
}

int Xml::removeAppData(unsigned int id)
{
	TiXmlDocument xml_doc;
	if (!xml_doc.LoadFile(APPDADAFILE))
	{
		return -1;
	}
	TiXmlElement*xml_root = xml_doc.RootElement();
	TiXmlElement* xml_infor = xml_root->FirstChildElement("infor");
	bool flag = false;
	while (xml_infor)
	{
		if (AfTinyXml::childAsIntU(xml_infor,"id")==id)
		{
			xml_root->RemoveChild(xml_infor);
			flag = true;
			break;
		}
		xml_infor = xml_infor->NextSiblingElement("infor");
	}
	xml_doc.SaveFile();
	if (!flag)
	{
		return -2;   //查询不到这个id
	}
	return 0;
}

int Xml::getAppData(std::map<unsigned int, AppPathData>& dataMap)
{
	TiXmlDocument xml_doc;
	if (!xml_doc.LoadFile(APPDADAFILE))
	{
		return -1;
	}
	TiXmlElement*xml_root = xml_doc.RootElement();
	TiXmlElement* xml_infor = xml_root->FirstChildElement("infor");
	while (xml_infor)
	{
		AppPathData d = { 0 };
		d.app_ioc_path = QString::fromStdString(AfTinyXml::childAsText(xml_infor, "icon"));
		d.app_name = QString::fromStdString(AfTinyXml::childAsText(xml_infor, "name"));
		d.app_path = QString::fromStdString(AfTinyXml::childAsText(xml_infor, "path"));
		dataMap.insert(std::map<unsigned int, AppPathData >::value_type(AfTinyXml::childAsIntU(xml_infor, "id"), d));
		xml_infor = xml_infor->NextSiblingElement("infor");
	}
	return 0;
}

Xml* Xml::g_xml=nullptr;
