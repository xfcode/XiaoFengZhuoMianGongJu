#ifndef BUTTON_H
#define BUTTON_H

#include <QPushButton>
#include "ui_Button.h"

class Button : public QPushButton
{
	Q_OBJECT

public:
	Button(QWidget *parent = 0);
	~Button();

private:
	Ui::Button ui;
};

#endif // BUTTON_H
