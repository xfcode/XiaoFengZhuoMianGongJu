#ifndef ADDAPP_H
#define ADDAPP_H

#include <QDialog>
#include "ui_AddApp.h"
#include "type.h"
class AddApp : public QDialog
{
	Q_OBJECT

public:
	AddApp(QWidget *parent = 0);
	~AddApp();


private:
	Ui::AddApp ui;

	int m_pag_main_curr;   //当前的 主窗口的页数  
	AppPathData data;

signals:
	void sendData(AppPathData);

private slots:
void nextButton();
void lastButton();
void finshButton();
void appPathButton();   //选着APP路径的按钮
void appIocButton();    //选着app图标的按钮

};

#endif // ADDAPP_H
